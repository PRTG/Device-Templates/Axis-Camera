## PRTG Device Template for Axis Camera devices

This project is a custom device template that cna be used to monitor a Axis Camera in PRTG using the auto-discovery for sensor creation.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/Axis-Camera/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md.
